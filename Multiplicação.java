package br.senai.sp.informatica.heranca;

public class Multiplicação extends OperacaoMatematica {
	
	@Override
	double calcular(double x, double y) {
		return x * y;
	}
}
