package br.senai.sp.informatica.heranca;

public abstract class Ave extends Animal {
	
	public Ave(double peso, String comida) {
		super(peso, comida);
	}

	void voar() {
		System.out.println("Voou...");
	}
	
	void botar() {
		System.out.println("Botou um ovo...");
	}
}
