package br.senai.sp.informatica.heranca;

public class AnimalTeste {

	public static void barulho(Animal a) {
		a.fazerBarulho();
	}
	
	public static void main(String[] args) {
		
		Cachorro toto = new Cachorro(10, "Carne");
		Galinha penosa = new Galinha(5, "Milho");
		Pombo pru = new Pombo(2, "P�o");
		Coruja c = new Coruja(5, "Rato");
		
		barulho(toto);
		barulho(penosa);
		barulho(pru);
		barulho(c);
		
		// teste de tipos
		//System.out.println(toto instanceof Cachorro);//true
		//System.out.println(toto instanceof Animal);//true
	}

}
