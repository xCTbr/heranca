package br.senai.sp.informatica.heranca;

public interface AreaCalculavel {

	double calculaArea();
}
