package br.senai.sp.informatica.heranca;

public class Cubo implements AreaCalculavel, VolumeCalculavel {

	double face;

	public Cubo(double face) {
		this.face = face;
	}

	@Override
	public double calculaArea() {
		return 6 * Math.pow(face, 2);
	}

	@Override
	public double calculaVolume() {
		// TODO Auto-generated method stub
		return Math.pow(face, 3);
	}


}
