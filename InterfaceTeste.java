package br.senai.sp.informatica.heranca;

public class InterfaceTeste {

	public static void area(AreaCalculavel a){
		System.out.println(a.calculaArea());
	}
	
	
	public static void main(String[] args) {

		area(new Quadrado(2));
		area(new Cubo(2));
		
		volume(new Cubo(2));
		
		
		Quadrado quadrado = new Quadrado(2);
		System.out.println("Area: " + quadrado.calculaArea());

		Cubo cubo = new Cubo(5);
		System.out.println("Area: " + cubo.calculaArea());
		System.out.println("Volume: " + cubo.calculaVolume());
	}

}
