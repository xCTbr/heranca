package br.senai.sp.informatica.heranca;

public abstract class Animal {
	
	//atributos
	protected double peso;
	protected String comida;
	
	//construtor
	public Animal(double peso, String comida) {
		this.peso = peso;
		this.comida = comida;
	}
	
	//M�todos
	void dormir() {
		System.out.println("Dormiu...");
	}
	abstract void fazerBarulho();
}
