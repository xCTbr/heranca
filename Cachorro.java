package br.senai.sp.informatica.heranca;

public class Cachorro extends Animal {
	
	public Cachorro(double peso, String comida) {
		super(peso, comida);
	}

	void enterrarOsso() {
		System.out.println("Enterrou um osso...");
	}
	
	@Override
	void fazerBarulho() {
		// super.fazerBarulho(); - Executa o metodo da classe 
		// pai ou se estiver no construtor ele chama o construtor 
		// da classe pai
		System.out.println("AUUAUAUAUAUAUAUAUUUUUU");
	}

}
