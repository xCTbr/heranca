package br.senai.sp.informatica.heranca;

public class OperacaoTeste {

	public static void calc(OperacaoMatematica o, double x, double y) {
		System.out.println(o.calcular(x, y));
	}
	
	public static void main(String[] args) {
		
		calc(new Soma(), 5, 10);
		
		calc(new Multiplicação(), 5, 10);

	}

}
